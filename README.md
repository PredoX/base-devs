Entorno de desarrollo con Angular y PhalconPHP
    - nginx:1.19.2-alpine servidor web
    - mariadb:10.5.5 base de datos
    - php:7.4-fpm-alpine3.12 con modulos PhalconPHP 4.0.5
    - node:14.11.0-alpine3.10 con @angular/cli:10.1.3

src/api: Carpeta backend en php
src/frontend: Carpeta desarrollo fronend con angular